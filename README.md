# AWS CLI 

## Table of Contents

- [Project Description](#project-description)

- [Technologies Used](#technologies-used)

- [Steps](#steps)

- [Installation](#installation)

- [How to Contribute](#how-to-contribute)

- [Questions](#questions)

- [References](#references)

- [License](#license)

## Project Description

* Installed and configured AWS CLI tool to connect to our AWS account

* Created EC2 Instance using the AWS CLI with all necessary configurations like Security Group

* Created SSH key pair

* Created IAM resources like User, Group, Policy using the AWS CLI

* Listed and browsed AWS resources using the AWS CL

## Technologies Used 

* AWS 

* Linux  


## Steps 

Step 1: Install aws client

     brew install awscli 

[Installed aws cli](/images/01_installed_aws_cli.png)

Step 2: Log into aws 

     aws config 

[already logged in](/images/02_aws_config_to_login.png)
[Credentials location](/images/03_aws_hidden_file_in_home_directory.png)

Step 3: Get vpc id 

     aws ec2 describe-vpcs

[VPC](/images/04_getting_vpc_id.png)

Step 4: Create security group 

     aws ec2 create-security-group --group-name my-sg --description "My SG" --vpc-id vpc-0b88da2b485fde145

[Created Security Group](/imaeges/05_creating_security_group.png)

Step 5: Check if security group has been created 

     aws ec2 describe-security-groups --group-ids sg-06aa47fc1

[Security Group info](/images/06_new_information_about_security_group.png)

Step 6: Open port 22 for ssh access 

     aws ec2 authority-security-group-ingress --group-id  sg-06aa47fc1ecac7728 --protocol tcp --port 22 --cidr 143.58.216.204/32

[Opening por 22](/images/07_opening_port22_for_ssh_access_for_my_security_group.png)
[SG on UI](/images/08_new_security_group_on_aws_UI.png)
[Inside SG](/images/09_inside_security_group.png)

Step 7: Create SSH key pair 

     aws ec2 create-key-pair --key-name MykpCli --query 'keyMaterial' --output text > MykpCli.pem 

[Key pair Creation](/images/10_creating_ssh_key_pair.png)

Step 8: Get Subnet id 

     aws ec2 describe-subnets 

[Subnets info](/images/11_getting_subnet_id.png)

Step 10: Run instance from aws cli 

     aws ec2 run-instances --image-id ami-0648ea225c13e0729 --count 1 --instance-type t2.micro --key-name MykpCli --security-group-ids sg-06aa47fc1ecac7728 --subnet-id subnet-0d454af95211c8f18

[Running Instance](/images/12_running_instance_from_aws_cli.png)
[Instance on AWS](/images/13_instance_on_aws_ui.png)

Step 11: Change permission on downloaded private key 

     mv ~/Downloads/MykpCli.pm ~/.ssh
     chmod 400 ~/.ssh/MykpCli.pm 

[Changed Permission](/images/15_chmod_400_changed_permissions.png)

Step 12: Ssh to ec2 instance to test if it works 

     ssh -i .ssh/MykpCli.pem ec2-user@13.40.167.66

[ssh into ec2 instance](/images/14_ssh_into_server.png)

Step 13: filter display all instances that is of type t2 micro

     aws ec2 describe-instances --filters "Name=instance-type,values=t2.micro" --query "Reservation[].Instances[].InstanceId"

[filter](/images/16_filter_display_all_instanceid_of_all_ec2_intance_type_t2_micro.png)

Step 14: Create a group 

     aws iam create-group --group-name MyGroupCli

[Created Group](/images/17_created_group.png)

Step 15: Create a user 

     aws iam create-user --user-name MyUserCli 

[Created User](/images/18_created_user.png)

Step 16: Assign user to group 

     aws iam add-user-to-group --user-name MyUserCli --group-name MyGroupCli 

[Assigning User to group](/images/19_assigning_users_to_group.png)
[info group](/images/20_info_about_group.png)

Step 17: Check policy you want to assign to group ARN on UI 

[Policy ARN](/images/21_policy_identify_on_ui.png)

Step 18: Attach policy to group 

     aws iam attach-group-policy --group-name --policy-arn arn:aws:iam:aws:policy/AmazonEC2FullAccess

[Attached policy](/images/22_attaching_policy_to_group.png)

Step 19: Verify if policy is attached 

     aws iam list-attached-group-policies --group-name MyGroupCli 

[Verifying policy](/images/23_verifying_if_its_attached.png)

Step 20: Create password for user and give user option after login to create their own passowrd 

     aws iam create-login-profile --user-name MyUserCli --password Mypwd123test --password-reset-required

[Created password](/images/24_creating_password_for_user.png)


Step 21: Get account id to test login 

     aws iam get-user --user-name MyUserCli 

[Account Id](/images/25_get_account_id_to_test_log_in_on_browswer.png)

Step 22: login with credentials created through aws cli 

[Password prompt](/images/26_password_prompt.png)

Note: This will not let you change passwords as you do not have iam policy for chaging password. The only policy that this user has is ec2 policies 

Step 23: Create json file  for iam getaccountpassword and changepassword policy 

[Json File](/images/27_creating_json_file.png)

Step 24: Create iam policy for password creation permission 

     aws iam create-policy --policy-name changePwd --policy-document file://changePwdPolicy.json 

[Created Policy](/images/28_created_iam_policy_for_password_creation_permission.png)


Step 25: Attach policy to group in which MyUserCli belongs to which is MyGroupCli 

     aws iam attach-group-policy --group-name MyGroupCli --policy-arn arn:aws:iam::522116691231:policy/changePwd

[Attaching policy to group](/images/29_attaching_policy_to_group_to_enable_user_to_have_policy.png)

Step 26: Test Now if you can change the password and sign in

[Signed into aws](/images/30_signed_into_aws.png)

Step 27: Create access key for user 

     aws iam create-access-key --user-name MyUserCli 

[Created Acces Key for user](/iamges/31_created_access_key_for_user.png)

Step 28: Temporay login as user through environmental variables 

     export AWS_ACCESS_KEY_ID=AKRFIDNDISNS
     export AWS_SECRET_KEY=Btyyifndodcc/fogd

[Environmental variables](/images/32_temporary_logged_in_as_user_through_environmental_variable.png)


Step 29: Test this by using commands that user shouldnt have access to

     aws iam create-user --user-name test

[Permission denied](/images/33_testing_if_its_working.png)

## Installation

Run $ brew install awscli

## How to Contribute

1. Clone the repo using $ git clone git@gitlab.com:omacodes98/aws-cli.git

2. Create a new branch $ git checkout -b your name 

3. Make Changes and test 

4. Submit a pull request with description for review



## Questions

Feel free to contact me for further questions via: 

Gitlab: https://gitlab.com/omacodes98

Email: omacodes98@gmail.com

## References

https://gitlab.com/omacodes98/aws-cli

## License

The MIT License 

For more informaation you can click the link below:

https://opensource.org/licenses/MIT

Copyright (c) 2022 Omatsola Beji.